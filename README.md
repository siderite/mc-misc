# mc-misc

Collection of miscellaneous functions that are related to Minecraft.

## Features

* Digest to Minecraft hex: treats the input bytes as one large integer in two's complement and writes the integer as base 16, placing a minus sign if the interpreted number is negative.

## Why

I mainly made this to learn some basic C.
It could be useful as a reference implementation to test against.
Or if you need to squeeze every last drop of performance out of your application.
