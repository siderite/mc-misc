#include "mc_hex_tests.h"
#include "../src/mc_hex.h"

#include <openssl/sha.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * mc_hex_test(char *data) {
    size_t length = strlen(data);
    unsigned char hash[SHA_DIGEST_LENGTH];
    char *str = (char *)malloc(MAX_HEX_LEN);

    SHA1((unsigned char *)data, length, hash);
    digest_to_hex(str, hash);
    return str;
}

START_TEST (notch)
{
    char *hex = mc_hex_test("Notch");
    ck_assert_str_eq(hex, "4ed1f46bbe04bc756bcb17c0c7ce3e4632f06a48");
    free(hex);
}
END_TEST

START_TEST (jeb_)
{
    char *hex = mc_hex_test("jeb_");
    ck_assert_str_eq(hex, "-7c9d5b0044c130109a5d7b5fb5c317c02b4e28c1");
    free(hex);
}
END_TEST

START_TEST (simon)
{
    char *hex = mc_hex_test("simon");
    ck_assert_str_eq(hex, "88e16a1019277b15d58faf0541e11910eb756f6");
    free(hex);
}
END_TEST

Suite * mc_hex_suite(void)
{
    Suite *s;
    TCase *tc_core;

    s = suite_create("MC hex");

    /* Core test case */
    tc_core = tcase_create("Core");

    tcase_add_test(tc_core, notch);
    tcase_add_test(tc_core, jeb_);
    tcase_add_test(tc_core, simon);
    suite_add_tcase(s, tc_core);

    return s;
}
