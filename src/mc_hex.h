#ifndef MC_HEX_DIGEST_H
#define MC_HEX_DIGEST_H

#include <stdlib.h>

/* tcc doesn't like it when this is a static const */
#define SHA1_LEN 20

/**
 * Max length, calculated with: (HASH_LEN * 2) + 2
 * the 2 extra bytes are the string terminator and a possible - sign
 */
#define MAX_HEX_LEN ((SHA1_LEN * 2) + 2) * sizeof(char)

/**
 * @param hash an array with a length of 'SHA1_LEN'
 * @return size of the C string at 'dst'
 */
size_t digest_to_hex(char *dst, unsigned char *hash);

/**
 * @param hash an array with a length of 'SHA1_LEN'
 */
void twos_compliment(unsigned char *hash);

#endif
