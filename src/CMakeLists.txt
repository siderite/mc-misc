cmake_minimum_required(VERSION 3.12)
project(mc-misc LANGUAGES C VERSION 0.0.1 DESCRIPTION "mc-misc")

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra -Weverything -Wstrict-aliasing -pedantic -fstrict-aliasing")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -flto -fslp-vectorize-aggressive")

set(SOURCES
    mc_hex.h
    mc_hex.c
)

add_library(mc-misc ${SOURCES})
target_compile_options(mc-misc PUBLIC -std=c89)
set_target_properties(mc-misc PROPERTIES VERSION ${PROJECT_VERSION})

set_target_properties(mc-misc PROPERTIES
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/../out"
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/../out"
)
