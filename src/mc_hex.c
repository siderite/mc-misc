#include "mc_hex.h"

size_t digest_to_hex(char *dst, unsigned char *hash) {
    const static char CHARS[] = "0123456789abcdef";
    char *start = dst;
    size_t i;

    /* Calculate two's complement when negative */
    if ((hash[0] & 0x80) == 0x80) {
        twos_compliment(hash);
        *(dst++) = '-';
    }

    /* Don't print leading zeros */
    for (i = 0; i < SHA1_LEN; i++) {
        if (hash[i] != 0) {
            break;
        }
    }

    if (hash[i] < 16) {
        *(dst++) = CHARS[hash[i]];
        /* We already wrote this byte,
           don't write it again in the next loop
        */
        i++;
    }

    for (; i < SHA1_LEN; i++) {
        *(dst++) = CHARS[hash[i] >> 4];
        *(dst++) = CHARS[hash[i] & 0x0f];
    }

    /* Write string terminator */
    *(dst++) = 0;

    /* Return string size, including string terminator */
    return (size_t)(dst - start);
}

void twos_compliment(unsigned char *hash) {
    char carry = 1;
    int i;

    for (i = (SHA1_LEN - 1); i >= 0; i--) {
        hash[i] = ~hash[i];
        if (carry) {
            carry = (hash[i] == 0xff);
            hash[i] = hash[i] + 1;
        }
    }
}
